
(function () {
	/**
	 * Load the sensors and display them to the user
	 */
	loadSensors();
	// Enable auomatic closing of sensor when we clos the popup
	var popup = document.getElementById("view");
	popup.addEventListener("popuphide", function(ev)
	{
		stopSensor(currentSensor);
	});
	window.addEventListener("tizenhwkey", function (ev) {
		page = document.getElementsByClassName("ui-page-active")[0];
		pageid = page ? page.id : "";
		console.log("This is the", pageid, "page");
		var activePopup = null,
			page = null,
			pageid = "";

		if (ev.keyName === "back") {
			activePopup = document.querySelector(".ui-popup-active");

			if (pageid === "main" && !activePopup) {
				try {
					tizen.application.getCurrentApplication().exit();
				} catch (ignore) {
				}
			} else {
				window.history.back();
			}
		}
	});
}());

/**
 * Global app state
 */
var sensors = null,
	currentSensor = null,
	sensorName = null;

/**
 * Get sensors
 * @returns Array
 */
function getSensors()  {
	if(!sensors) {
		if(window.tizen && tizen.hasOwnProperty('sensorservice')) {
			sensors = tizen.sensorservice.getAvailableSensors();
		}
	}
	return sensors;
}

/**
 * Load all of the sensors available to the system
 * @returns void
 */
function loadSensors(){
	stopSensor(currentSensor);
    var sensors = getSensors(),
    	list = document.getElementById("snapList");
    if(!sensors) {
		var li = document.createElement("li");
		li.appendChild(document.createTextNode("No sensors"));
		list.appendChild(li);
    } else {
    	if(list.firstElementChild) {
	    	console.log("Removing first child", list.firstElementChild, "From", list);
		    list.removeChild(list.firstElementChild);
    	}
	    sensors.map(function (name) {
			console.log("Searching for sensor", name);
			var sensor = tizen.sensorservice.getDefaultSensor(name);
			console.log("Found sensor:", sensor);
			var li = document.createElement("li");
			var link = document.createElement('a');
			link.href = "#view?sensor="+sensor.sensorType;
			link.innerHTML = sensor.sensorType;
			link.classList.add('ui-btn');
			link.dataset.rel = 'popup';
			link.addEventListener('click', function() {
			   startSensor(name);
			});
			li.appendChild(link);
			console.log("Appending item", li);
			list.appendChild(li);
	    });
    }
}

/**
 * Stop the current sensor
 * @param currentSensor
 * @returns
 */
function stopSensor(currentSensor) {
	// If we have a sensor already active stop it and nullify the value
	if(currentSensor) {
		currentSensor.unsetChangeListener();
		currentSensor.stop();
		currentSensor = sensorName = null;
	}
}

/**
 * Start the sgive sensor and collect data
 * @param name
 * @param sensor
 * @returns
 */
function startSensor(name) {
	var sensor = tizen.sensorservice.getDefaultSensor(name);
	document.querySelector('#view-header').innerHTML = name + ' Data';
	currentSensor = sensor;
	const list = document.querySelector('#view .ui-listview');
	sensor.setChangeListener(function (data) {
		console.log("Sensor", sensor, "Provided", data);
		while(list.firstElementChild) {
			list.firstElementChild.remove();
		}
		Object.keys(data).map(function (value, key) {
			var li = document.createElement("li");
			li.classList.add('li-has-multiline');
			li.innerHTML = "<strong>"+key+"</strong><br><span class='li-text-sub'>"+value+"</span>";
			list.appendChild(li);
		});
	});
	sensor.start(function () {
		console.log("Started sensor", sensor);
	});
}